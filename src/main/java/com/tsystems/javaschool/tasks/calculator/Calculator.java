package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.*;

public class Calculator {
    static boolean theGreatChecker = false;

//    public static void main(String[] args) {
//        String s = "-1-(12/((6+3)-10)*2)";
//        System.out.println(s + " = " + evaluate(s));
//    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static String evaluate(String statement) {

        // Empty statement validation
        if ((statement == null) || (statement.equals(""))) return null;

        Character[] expressionInCharArray = statement.chars().mapToObj(s -> (char) s).toArray(Character[]::new);
        String[] expressionInStringArray = new String[expressionInCharArray.length];

        // Startsymbol validation
        if (!(expressionInCharArray[0].equals('(') || expressionInCharArray[0].equals('-') ||
                expressionInCharArray[0].equals('1') || expressionInCharArray[0].equals('2') ||
                expressionInCharArray[0].equals('3') || expressionInCharArray[0].equals('4') ||
                expressionInCharArray[0].equals('5') || expressionInCharArray[0].equals('6') ||
                expressionInCharArray[0].equals('7') || expressionInCharArray[0].equals('8') ||
                expressionInCharArray[0].equals('9') || expressionInCharArray[0].equals('0'))) {
            return null;
        }

        int i = 0;
        for (Character character : expressionInCharArray) {
            if (character.equals('1') || character.equals('2') || character.equals('3') || character.equals('4') ||
                    character.equals('5') || character.equals('6') || character.equals('7') || character.equals('8') ||
                    character.equals('9') || character.equals('0') || character.equals('.')) {
                if (expressionInStringArray[i] == null) {
                    expressionInStringArray[i] = character.toString();
                } else {
                    expressionInStringArray[i] += character;
                }
            } else if (character.equals('*') || character.equals('/') ||
                    character.equals('+') || character.equals('-') || character.equals('(') || character.equals(')')) {
                if (expressionInStringArray[i] == null) {
                    expressionInStringArray[i] = character.toString();
                } else expressionInStringArray[++i] = character.toString();
                i++;
            } else {
                return null;
            }
        }

        List<String> expressionInList = new ArrayList<>(Arrays.asList(expressionInStringArray));

        //For start with a negative value
        if (expressionInList.get(0).equals("-")) {
            expressionInList.set(0, "-1");
            expressionInList.add(1, "*");
        }

        expressionInList.removeIf(Objects::isNull);

        //Found bracket indexes
        List<Integer> indexOfOpenBr = new ArrayList<>();
        List<Integer> indexOfCloseBr = new ArrayList<>();
        i = 0;
        for (String s : expressionInList) {
            if ("(".equals(s)) {
                indexOfOpenBr.add(i);
            }
            if (")".equals(s)) {
                indexOfCloseBr.add(i);
            }
            i++;
        }

        while ((expressionInList.size() != 1) && (!theGreatChecker)) {
            expressionInList = removeParentheses(expressionInList, indexOfOpenBr, indexOfCloseBr);
            //System.out.println("выражение после удаления скобок" + expressionInList);
        }

        String resultString;

        if ((expressionInList.get(0) != null) && (!theGreatChecker)) {
            double result = Double.valueOf(expressionInList.get(0));
            resultString = new DecimalFormat("#.####").format(result).replace(',', '.');
        } else {
            resultString = null;
        }

        return resultString;
    }

    private static List<String> removeParentheses(List<String> expressionInList, List<Integer> indexOfOpenBr, List<Integer> indexOfCloseBr) {
        List<String> copyOfExpressionInList = new ArrayList<>(expressionInList);
        int begIndex = expressionInList.indexOf("(");
        int endIndex = expressionInList.indexOf(")");

        if (begIndex == -1 && endIndex == -1) {
            //System.out.println("выдаем арифметику");
            copyOfExpressionInList = arithmeticOperation(copyOfExpressionInList);
            return copyOfExpressionInList;

        } else if ((begIndex != -1 && endIndex == -1) || (begIndex == -1 && endIndex != -1)) {
            //System.out.println("ERROR: amount of '(' and ')' is incorrect");
            theGreatChecker = true;
            expressionInList.clear();
            expressionInList.add(null);
            return expressionInList;
        }
        if (begIndex != -1 && endIndex != -1) {
            if (begIndex > endIndex) {
                //System.out.println("ERROR: ) before (");
                theGreatChecker = true;
                expressionInList.clear();
                expressionInList.add(null);
                return expressionInList;
            } else {
                if (begIndex == copyOfExpressionInList.lastIndexOf("(")) {
                    //System.out.println("indexOfOpen and Close" + indexOfOpenBr + " " + indexOfCloseBr);


                    //System.out.println("next step remove ()");
                    copyOfExpressionInList = removeParentheses(copyOfExpressionInList.subList(begIndex + 1, endIndex), indexOfOpenBr, indexOfCloseBr);
                    //System.out.println(" for me exprList" + expressionInList + " copy "
//                            + copyOfExpressionInList + " indexes open " + indexOfOpenBr + " close " + indexOfCloseBr +
//                            " beg and end Ind " + begIndex + " " + endIndex);


                    indexOfOpenBr.remove((Object) begIndex);
                    indexOfCloseBr.remove((Object) endIndex);
                    //System.out.println("indexOfOpen and Close after removing" + indexOfOpenBr + " " + indexOfCloseBr);

                    if (endIndex > begIndex) {
                        expressionInList.subList(begIndex, endIndex).clear();
                    }
                    expressionInList.set(begIndex, copyOfExpressionInList.get(0));

                } else {
//                    System.out.println("indexOfOpen and Close in lastblock" + indexOfOpenBr + " " + indexOfCloseBr);

                    Iterator<Integer> iteratorOfIndex = indexOfOpenBr.iterator();

                    while (iteratorOfIndex.hasNext()) {
                        Integer i = iteratorOfIndex.next();

                        if (indexOfCloseBr.get(0) < i) {
                            begIndex = indexOfOpenBr.get(indexOfOpenBr.indexOf(i) - 1);

                            copyOfExpressionInList = removeParentheses
                                    (copyOfExpressionInList.subList(begIndex + 1, endIndex), indexOfOpenBr, indexOfCloseBr);

//                            System.out.println(indexOfOpenBr.indexOf(i) + " при условии что открывающая скобка после закрывающей" +
//                                    ", индекс соответствующий итерации");
                            break;
                        }

                        if (i.equals(indexOfOpenBr.get(indexOfOpenBr.size() - 1))) {
//                            System.out.println(indexOfOpenBr.indexOf(i) + " при условии что открывающей скобки после закрывающей" +
//                                    " нет, индекс соответствующий итерации");
                            begIndex = i;

                            copyOfExpressionInList = removeParentheses
                                    (copyOfExpressionInList.subList(begIndex + 1, endIndex), indexOfOpenBr, indexOfCloseBr);
                        }
                    }

                    indexOfOpenBr.remove((Object) begIndex);
                    indexOfCloseBr.remove((Object) endIndex);

                    for (int j = begIndex; j < endIndex; j++) {
                        expressionInList.remove(begIndex);
                    }
                    expressionInList.set(begIndex, copyOfExpressionInList.get(0));

//                    System.out.println("I NOT SURE exprList" + expressionInList + " copy "
//                            + copyOfExpressionInList + " indexes open " + indexOfOpenBr + " close " + indexOfCloseBr +
//                            " beg and end Ind " + begIndex + " " + endIndex);
                }
            }
        }
        return expressionInList;
    }

    private static List<String> arithmeticOperation(List<String> listWithoutParentheses) {

        if (listWithoutParentheses.size() == 1) {
            return listWithoutParentheses;
        }

        try {
            if ((listWithoutParentheses.indexOf("*") != -1) &&
                    ((listWithoutParentheses.indexOf("*") < listWithoutParentheses.indexOf("/")) ||
                            (listWithoutParentheses.indexOf("/") == -1))) {
                int indexOfOperator = listWithoutParentheses.indexOf("*");
                listWithoutParentheses.set(indexOfOperator - 1,
                        Double.toString(Double.parseDouble(listWithoutParentheses.get(indexOfOperator - 1)) *
                                Double.parseDouble(listWithoutParentheses.get(indexOfOperator + 1))));
                listWithoutParentheses.remove(indexOfOperator);
                listWithoutParentheses.remove(indexOfOperator);
                arithmeticOperation(listWithoutParentheses);
            }

            if ((listWithoutParentheses.indexOf("/") != -1) &&
                    ((listWithoutParentheses.indexOf("/") < listWithoutParentheses.indexOf("*")) ||
                            (listWithoutParentheses.indexOf("*") == -1))) {

                int indexOfOperator = listWithoutParentheses.indexOf("/");

                // Division by zero validation
                if ((Double.parseDouble(listWithoutParentheses.get(indexOfOperator + 1)) == 0)) {
                    theGreatChecker = true;
                    listWithoutParentheses.clear();
                    listWithoutParentheses.add(null);
                    return listWithoutParentheses;
                }
                listWithoutParentheses.set(indexOfOperator - 1,
                        Double.toString(Double.parseDouble(listWithoutParentheses.get(indexOfOperator - 1)) /
                                Double.parseDouble(listWithoutParentheses.get(indexOfOperator + 1))));
                listWithoutParentheses.remove(indexOfOperator);
                listWithoutParentheses.remove(indexOfOperator);
                arithmeticOperation(listWithoutParentheses);
            }

            if ((listWithoutParentheses.indexOf("+") != -1) &&
                    ((listWithoutParentheses.indexOf("+") < listWithoutParentheses.indexOf("-")) ||
                            (listWithoutParentheses.indexOf("-") == -1))) {
                int indexOfOperator = listWithoutParentheses.indexOf("+");
                listWithoutParentheses.set(indexOfOperator - 1,
                        Double.toString(Double.parseDouble(listWithoutParentheses.get(indexOfOperator - 1)) +
                                Double.parseDouble(listWithoutParentheses.get(indexOfOperator + 1))));
                listWithoutParentheses.remove(indexOfOperator);
                listWithoutParentheses.remove(indexOfOperator);
                arithmeticOperation(listWithoutParentheses);
            }

            if ((listWithoutParentheses.indexOf("-") != -1) &&
                    ((listWithoutParentheses.indexOf("-") < listWithoutParentheses.indexOf("+")) ||
                            (listWithoutParentheses.indexOf("+") == -1))) {
                int indexOfOperator = listWithoutParentheses.indexOf("-");
                listWithoutParentheses.set(indexOfOperator - 1,
                        Double.toString(Double.parseDouble(listWithoutParentheses.get(indexOfOperator - 1)) -
                                Double.parseDouble(listWithoutParentheses.get(indexOfOperator + 1))));
                listWithoutParentheses.remove(indexOfOperator);
                listWithoutParentheses.remove(indexOfOperator);
                arithmeticOperation(listWithoutParentheses);
            }
        } catch (NumberFormatException NFE) {
            theGreatChecker = true;
            listWithoutParentheses.clear();
            listWithoutParentheses.add(null);
        }
        return listWithoutParentheses;
    }
}
