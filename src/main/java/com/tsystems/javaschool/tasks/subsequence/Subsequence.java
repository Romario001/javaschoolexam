package com.tsystems.javaschool.tasks.subsequence;


import java.util.Iterator;
import java.util.List;


public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public static boolean find(List x, List y) {
        boolean result = false;

        try {
            if ((x.isEmpty()) && (y.isEmpty() || true)) {
                return true;
            }

            if (y.isEmpty()) {
                return false;
            }

            if (!(x.get(0).getClass().equals(y.get(0).getClass()))) {
                return false;
            }

            int checkIndex = x.size();
//            System.out.println("Initial check " + checkIndex);
            Iterator<Integer> yIterator = y.iterator();

            for (int i = 0; i < x.size(); i++) {

                while (yIterator.hasNext()) {
                    Object object = yIterator.next();
//                    System.out.println("y " + y);
                    if (object.equals(x.get(i))) {
                        yIterator.remove();
                        checkIndex--;
                        System.out.println(checkIndex);
                        break;
                    } else
                        yIterator.remove();
//                    System.out.println("y after remove " + y);
                }
            }
//            System.out.println("Result check " + checkIndex);
            if (checkIndex == 0) {
                result = true;
            }
        } catch (NullPointerException NPE) {
            throw new IllegalArgumentException();
        }
        return result;
    }
}
