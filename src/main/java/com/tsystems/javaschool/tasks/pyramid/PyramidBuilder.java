package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        //Validation of inputNumbers list
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int listSize = inputNumbers.size();
        int correctCountIndex = 0;
        int countOfStrings = 0;
        int countOfColumns = 0;
        int shiftInColumnIndex = 0;

        while (listSize > 0) {
            correctCountIndex++;
            listSize -= correctCountIndex;
            countOfStrings++;
        }
        countOfColumns = 2 * countOfStrings - 1;
        System.out.println(countOfStrings + " ");
        if (countOfStrings >= Math.pow(2, 16)) {
            throw new CannotBuildPyramidException();
        }

        //Sort array
        Collections.sort(inputNumbers);

        //Fill the array with zeros
        int[][] resultArray = new int[countOfStrings][countOfColumns];
        for (int i = 0; i < countOfStrings; i++) {
            for (int j = 0; j < countOfColumns; j++) {
                resultArray[i][j] = 0;
            }
        }

        if (listSize != 0) {
            throw new CannotBuildPyramidException();
        } else {
            listSize = inputNumbers.size();

//            System.out.println("Strings: " + countOfStrings + " Columns " + countOfColumns);
//
//            for (int i = 0; i < resultArray.length; i++) {
//                for (int j = 0; j < resultArray[i].length; j++) {
//                    System.out.print(resultArray[i][j] + "\t");
//                }
//                System.out.println();
//            }

            for (int i = countOfStrings - 1; i >= 0; i--) {
                for (int j = countOfColumns - 1; j >= 0; j -= 2) {
                    resultArray[i][j + shiftInColumnIndex] = inputNumbers.get(listSize - 1);
                    listSize--;
                }
                countOfColumns -= 2;
                shiftInColumnIndex++;
            }
        }

        return resultArray;
    }


}
